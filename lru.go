package diskv

import (
	"container/list"
	"errors"
	"strings"
	"sync"
)

// EvictCallback is used to get a callback when a cache entry is evicted
type EvictCallback func(key string, value []byte)

// LRU implements a non-thread safe fixed size LRU cache
type LRU struct {
	size          int
	maxMemory     uint64
	currentMemory uint64
	evictList     *list.List
	items         map[string]*list.Element
	onEvict       EvictCallback

	lrw sync.RWMutex
}

// entry is used to hold a value in the evictList
type entry struct {
	key    string
	value  []byte
	memory uint64
}

// NewLRU constructs an LRU of the given size
func NewLRU(size int, maxMemory uint64, onEvict EvictCallback) (*LRU, error) {
	if size <= 0 {
		return nil, errors.New("must provide a positive size")
	}
	c := &LRU{
		currentMemory: 0,
		maxMemory:     maxMemory,
		size:          size,
		evictList:     list.New(),
		items:         make(map[string]*list.Element),
		onEvict:       onEvict,
	}
	return c, nil
}

// Purge is used to completely clear the cache.
func (c *LRU) Purge() {
	c.lrw.Lock()
	defer c.lrw.Unlock()

	for k, v := range c.items {
		if c.onEvict != nil {
			c.onEvict(k, v.Value.(*entry).value)
		}
		delete(c.items, k)
	}
	c.evictList.Init()
	c.currentMemory = 0
}

func (c *LRU) PurgePrefix(prefix string) {
	c.lrw.Lock()
	defer c.lrw.Unlock()

	for k, v := range c.items {
		if strings.HasPrefix(k, prefix) {
			if c.onEvict != nil {
				c.onEvict(k, v.Value.(*entry).value)
			}
			c.removeElement(v) // 删除
			delete(c.items, k)
		}
	}
}

// Add adds a value to the cache.  Returns true if an eviction occurred.
func (c *LRU) Add(key string, value []byte) (evicted bool) {
	c.lrw.Lock()
	defer c.lrw.Unlock()

	// Check for existing item
	if ent, ok := c.items[key]; ok {
		c.evictList.MoveToFront(ent)
		ent.Value.(*entry).value = value
		return false
	}

	// Add new item
	ent := &entry{key, value, uint64(len(value))}
	entry := c.evictList.PushFront(ent)
	c.items[key] = entry

	c.currentMemory += ent.memory
	evict := c.evictList.Len() > c.size
	// Verify size not exceeded
	if evict || c.currentMemory >= c.maxMemory {
		c.removeOldest() // 清理旧的数据
	}
	return evict
}

// Get looks up a key's value from the cache.
func (c *LRU) Get(key string) (value []byte, ok bool) {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	if ent, ok := c.items[key]; ok {
		c.evictList.MoveToFront(ent)
		if ent.Value.(*entry) == nil {
			return nil, false
		}
		return ent.Value.(*entry).value, true
	}
	return
}

// Contains checks if a key is in the cache, without updating the recent-ness
// or deleting it for being stale.
func (c *LRU) Contains(key string) (ok bool) {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	_, ok = c.items[key]
	return ok
}

// Peek returns the key value (or undefined if not found) without updating
// the "recently used"-ness of the key.
func (c *LRU) Peek(key string) (value []byte, ok bool) {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	var ent *list.Element
	if ent, ok = c.items[key]; ok {
		return ent.Value.(*entry).value, true
	}
	return nil, ok
}

// Remove removes the provided key from the cache, returning if the
// key was contained.
func (c *LRU) Remove(key string) (present bool) {
	c.lrw.Lock()
	defer c.lrw.Unlock()

	if ent, ok := c.items[key]; ok {
		c.removeElement(ent)
		return true
	}
	return false
}

// RemoveOldest removes the oldest item from the cache.
func (c *LRU) RemoveOldest() (key string, value []byte, ok bool) {
	c.lrw.Lock()
	defer c.lrw.Unlock()

	ent := c.evictList.Back()
	if ent != nil {
		c.removeElement(ent)
		kv, ok := ent.Value.(*entry)
		if ok {
			return kv.key, kv.value, true
		}
	}
	return "", nil, false
}

// GetOldest returns the oldest entry
func (c *LRU) GetOldest() (key string, value []byte, ok bool) {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	ent := c.evictList.Back()
	if ent != nil {
		kv, ok := ent.Value.(*entry)
		if ok {
			return kv.key, kv.value, true
		}
	}
	return "", nil, false
}

// Keys returns a slice of the keys in the cache, from oldest to newest.
func (c *LRU) Keys() []string {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	keys := make([]string, len(c.items))
	i := 0
	for ent := c.evictList.Back(); ent != nil; ent = ent.Prev() {
		keys[i] = ent.Value.(*entry).key
		i++
	}
	return keys
}

// Len returns the number of items in the cache.
func (c *LRU) Len() int {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	return c.evictList.Len()
}

// Resize changes the cache size.
func (c *LRU) Resize(size int) (evicted int) {
	c.lrw.RLock()
	defer c.lrw.RUnlock()

	diff := c.Len() - size
	if diff < 0 {
		diff = 0
	}
	for i := 0; i < diff; i++ {
		c.removeOldest()
	}
	c.size = size
	return diff
}

// removeOldest removes the oldest item from the cache.
func (c *LRU) removeOldest() {
	ent := c.evictList.Back()
	if ent != nil {
		c.removeElement(ent)
	}
}

// removeElement is used to remove a given list element from the cache
func (c *LRU) removeElement(e *list.Element) {
	c.evictList.Remove(e)
	kv, ok := e.Value.(*entry)
	if ok {
		c.currentMemory -= uint64(len(kv.value))

		delete(c.items, kv.key)
		if c.onEvict != nil {
			c.onEvict(kv.key, kv.value)
		}
	}
}
