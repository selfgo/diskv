package diskv

import (
	"fmt"
	"math/rand"
	"testing"
)

func genLRUKeys() []string {
	keys := make([]string, keyCount)
	for i := 0; i < 512; i++ {
		keys[i] = fmt.Sprintf("%d", i)
	}
	return keys
}

func genLRUValue(size int) []byte {
	v := make([]byte, size)
	for i := 0; i < size; i++ {
		v[i] = uint8((rand.Int() % 26) + 97) // a-z
	}
	return v
}

func Test_LruCacheLimit(t *testing.T) {
	lruCache, _ := NewLRU(10, 204088, func(key string, value []byte) {

	})
	keys := genKeys()
	values := genValue(1024)

	for i := 0; i < 20; i++ {
		lruCache.Add(keys[i], values)
	}

	if lruCache.Len() > 10 {
		t.Fail()
	}
}

func Test_LruCacheLimi1tMemory(t *testing.T) {
	lruCache, _ := NewLRU(10240, 10*1024, func(key string, value []byte) {

	})
	keys := genKeys()
	values := genValue(1024)

	for i := 0; i < 30; i++ {
		lruCache.Add(keys[i], values)
	}

	if lruCache.Len() > 10 {
		t.Fail()
	}
}
