module github.com/koangel/diskv

go 1.12

require (
	github.com/google/btree v1.0.0
	github.com/koangel/ccache v1.0.2-0.20200401161119-5c7acab318af // indirect
	github.com/peterbourgon/diskv/v3 v3.0.0
)
